import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:oktoast/oktoast.dart';
import 'package:apdi/ui/widgets/dialogs/info_dialog.dart';
import 'package:apdi/ui/widgets/dialogs/loading_dialog.dart';

class DialogUtils {
  static bool isLoadingOpen = false;

  // closePreDialog is for closing dialog before opening new one
  static void showInfo(
    String message, {
    String buttonText = "OK",
    Function? onClick,
    bool closeOnOk = true,
    bool closePreDialog = false,
  }) {
    if (closePreDialog) closeDialog();
    Get.dialog(
      InfoDialog(
        text: message,
        onClickOK: () {
          if (onClick != null) {
            if (closeOnOk) {
              Get.back();
            }
            onClick();
          } else {
            Get.back();
          }
        },
        icon: FontAwesomeIcons.circleCheck,
        clickText: buttonText,
      ),
      barrierDismissible: false,
    );
  }

  static void showWarning(
    String message, {
    String buttonText = "OK",
    Function? onClick,
    bool closeOnOk = true,
    bool closePreDialog = false,
  }) {
    if (closePreDialog) closeDialog();
    Get.dialog(
      InfoDialog(
        text: message,
        onClickOK: () {
          if (onClick != null) {
            if (closeOnOk) {
              Get.back();
            }
            onClick();
          } else {
            Get.back();
          }
        },
        icon: FontAwesomeIcons.circleExclamation,
        clickText: buttonText,
      ),
      barrierDismissible: false,
    );
  }

  static void showChoose(
    String message,
    String buttonText, {
    Function? onClick,
    bool closeOnOk = true,
    bool closePreDialog = false,
  }) {
    if (closePreDialog) closeDialog();
    Get.dialog(
      InfoDialog(
        text: message,
        onClickOK: () {
          if (onClick != null) {
            if (closeOnOk) {
              Get.back();
            }
            onClick();
          } else {
            Get.back();
          }
        },
        icon: FontAwesomeIcons.circleCheck,
        onClickCancel: () => Get.back(),
        clickText: buttonText,
      ),
      barrierDismissible: false,
    );
  }

  static void showLoading(
    String message, {
    bool closePreDialog = false,
  }) {
    isLoadingOpen = true;
    if (closePreDialog) closeDialog();
    Get.dialog(
      LoadingDialog(text: message),
      barrierDismissible: false,
    );
  }

  static void showWarningToast(String text) {
    showToastWidget(
      Container(
        height: 80,
        width: 160,
        padding: const EdgeInsets.fromLTRB(8, 16, 8, 8),
        decoration: BoxDecoration(
          color: Colors.black54,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            const Icon(FontAwesomeIcons.circleExclamation,
                color: Colors.white, size: 24),
            const SizedBox(height: 8),
            Text(text, style: const TextStyle(fontSize: 12)),
          ],
        ),
      ),
      position: ToastPosition.center,
    );
  }

  static void closeDialog() {
    isLoadingOpen = false;
    Get.back();
  }
}
