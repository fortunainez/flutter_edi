import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class StorageUtils {
  static StorageUtils get to => Get.find();

  GetStorage storage = GetStorage();

  saveToken(String token) => storage.write('token', token);
  saveDevice(String device) => storage.write('device', device);
  get token => storage.read('token');
  get device => storage.read('device');
}