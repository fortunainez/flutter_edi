import 'package:flash/flash.dart';
import 'package:flutter/material.dart';

class FlashUtils {
  static void warningFlash(
      {FlashBehavior style = FlashBehavior.floating,
      required BuildContext context,
      String? title,
      String? message}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 2),
      builder: (_, controller) {
        return FlashBar(
          controller: controller,
          behavior: style,
          backgroundColor: const Color.fromRGBO(12, 71, 160, 1.0),
          position: FlashPosition.top,
          content: Text(
            message!,
            style: const TextStyle(color: Colors.white),
          ),
          title: Text(
            title!,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white),
          ),
        );
      },
    );
  }

  static void successFlash(
      {FlashBehavior style = FlashBehavior.floating,
      required BuildContext context,
      String? title,
      String? message}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 2),
      builder: (_, controller) {
        return FlashBar(
          controller: controller,
          behavior: style,
          backgroundColor: const Color.fromRGBO(0, 166, 90, 1.0),
          position: FlashPosition.top,
          content: Text(
            message!,
            style: const TextStyle(color: Colors.white),
          ),
          title: Text(
            title!,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white),
          ),
        );
      },
    );
  }

  static void infoFlash(
      {FlashBehavior style = FlashBehavior.floating,
      required BuildContext context,
      String? title,
      String? message}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 2),
      builder: (_, controller) {
        return FlashBar(
          controller: controller,
          behavior: style,
          backgroundColor: const Color.fromRGBO(0, 192, 239, 1.0),
          position: FlashPosition.top,
          content: Text(
            message!,
            style: const TextStyle(color: Colors.white),
          ),
          title: Text(
            title!,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white),
          ),
        );
      },
    );
  }

  static void defaultFlash(
      {FlashBehavior style = FlashBehavior.floating,
      required BuildContext context,
      String? title,
      String? message}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 2),
      builder: (_, controller) {
        return FlashBar(
          controller: controller,
          behavior: style,
          backgroundColor: const Color.fromRGBO(244, 244, 244, 1.0),
          position: FlashPosition.top,
          content: Text(
            message!,
            style: const TextStyle(color: Colors.black),
          ),
          title: Text(
            title!,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.black),
          ),
        );
      },
    );
  }

  static void dangerFlash(
      {FlashBehavior style = FlashBehavior.floating,
      required BuildContext context,
      String? title,
      String? message}) {
    showFlash(
      context: context,
      duration: const Duration(seconds: 2),
      builder: (_, controller) {
        return FlashBar(
          controller: controller,
          behavior: style,
          backgroundColor: const Color.fromRGBO(221, 75, 57, 1.0),
          position: FlashPosition.top,
          content: Text(
            message!,
            style: const TextStyle(color: Colors.white),
          ),
          title: Text(
            title!,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white),
          ),
        );
      },
    );
  }
}
