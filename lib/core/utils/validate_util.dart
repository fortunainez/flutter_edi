import 'package:flutter/material.dart';

class ValidateUtils {
  // RegEx pattern for validating email addresses.
  static String emailPattern =
      r"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$";
  static RegExp emailRegEx = RegExp(emailPattern);

  // Validates an email address.
  static bool isEmail(String value) {
    if (emailRegEx.hasMatch(value.trim())) {
      return true;
    }
    return false;
  }

  /*
   * Returns an error message if email does not validate.
   */
  static String? validateEmail(String value) {
    String email = value.trim();
    if (email.isEmpty) {
      return 'Email harus diisi.';
    }
    if (!isEmail(email)) {
      return 'Email tidak valid.';
    }
    return null;
  }

  /*
   * Returns an error message if required field is empty.
   */
  static String? requiredField(String value, String message) {
    if (value.trim().isEmpty) {
      return message;
    }
    return null;
  }

  static String? validateConfirmPassword(
      TextEditingController pw1, TextEditingController pw2) {
    if (pw2.text.trim().isEmpty) {
      return 'Konfirmasi kata sandi baru harus diisi';
    }
    if (pw1.text.trim() != pw2.text.trim()) {
      return 'Password tidak sama';
    }
    return null;
  }

  static String? validatePin(String value) {
    String pin = value.trim();
    if (pin.isEmpty) {
      return 'Pin harus diisi.';
    }
    if (pin.length != 4) {
      return 'Pin harus 4 digit.';
    }
    return null;
  }

  static String? validateMobile(String value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{8,13}$)';
    RegExp regExp = RegExp(patttern);
    if (value.isEmpty) {
      return 'No handphone harus diisi';
    } else if (!regExp.hasMatch(value)) {
      return 'No handphone tidak sesuai';
    }
    return null;
  }

   static String? validateIdPel(String value) {
    String patttern = r'[0-9]';
    RegExp regExp = RegExp(patttern);
    if (value.isEmpty) {
      return 'ID Pelanggan harus diisi';
    } else if (!regExp.hasMatch(value)) {
      return 'ID Pelanggan tidak sesuai';
    }
    return null;
  }
}
