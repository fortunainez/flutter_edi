import 'package:flutter/material.dart';

const String baseURL = 'http://192.168.208.193:8000/';

class Styles {
  static const primaryColor = Color(0xFF0C47A0);
  static const primaryRed = Color(0xFFC00000);
  static const primaryGreen = Color(0xFF00FFC2);
}
