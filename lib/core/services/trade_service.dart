import 'package:apdi/core/models/api_response_model.dart';
import 'package:apdi/core/models/trade_model.dart';
import 'package:apdi/core/services/http_connection.dart';

class TradeService extends HttpConnection {
  Future list(
  ) async {
    ApiResponseModel res = await get('https://618aa45c34b4f400177c47eb.mockapi.io/api/v1/Trade');
    if (res.status == 200) {
      List<TradeModel> data = [];
      res.data.forEach((el) {
        data.add(TradeModel.fromJson(el));
      });
      return data;
    }
  }

}
