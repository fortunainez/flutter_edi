import 'package:apdi/core/models/api_response_model.dart';
import 'package:apdi/core/models/slider_model.dart';
import 'package:apdi/core/services/http_connection.dart';

class SliderService extends HttpConnection {
  Future list(
  ) async {
    ApiResponseModel res = await get('https://618aa45c34b4f400177c47eb.mockapi.io/api/v1/Slider');
    if (res.status == 200) {
      List<SliderModel> data = [];
      res.data.forEach((el) {
        data.add(SliderModel.fromJson(el));
      });
      return data;
    }
  }

}
