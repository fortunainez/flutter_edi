import 'dart:ffi';

import 'package:apdi/core/models/employee_model.dart';
import 'package:dio/dio.dart';
import 'package:apdi/core/services/http_connection.dart';
import 'package:apdi/core/models/api_response_model.dart';
import 'package:apdi/core/models/user_model.dart';

class UserService extends HttpConnection {
  Future login(
    String email,
    String password,
  ) async {
    FormData data = FormData.fromMap({
      "email": email,
      "password": password,
    });

    return await post('/login', data: data, needsToken: false, pure: true);
  }

  Future register(
    String name,
    String email,
    String password,
  ) async {
    FormData data = FormData.fromMap({
      "name": name,
      "email": email,
      "password": password,
    });

    return await post('/register', data: data, needsToken: false, pure: true);
  }

Future updatestatus(
    int userId,
    int statusActive,
  ) async {
    FormData data = FormData.fromMap({
      "user_id": userId,
      "status_active": statusActive ,
    });

    return await post('/user/update', data: data, needsToken: false, pure: true);
  }
Future deleteemployee(
    int employeeId
  ) async {
    FormData data = FormData.fromMap({
      "id": employeeId,
    });

    return await post('/employee/delete',
        data: data, needsToken: false, pure: true);
  }

  Future me() async {
    return await get('/me', pure: true);
  }

  Future list() async {
    ApiResponseModel res = await get('/userlist');
   
    if (res.status == 200) {
      List<UserModel> data = [];
      res.data.forEach((el) {
        data.add(UserModel.fromJson(el));
      });
      return data;
    }
  }
   Future listemployee() async {
    ApiResponseModel res = await get('/employee/list');

    if (res.status == 200) {
      List<EmployeeModel> data = [];
      res.data.forEach((el) {
        data.add(EmployeeModel.fromJson(el));
      });
      return data;
    }
  }
  Future updateemployee(
    String position,
    String name,
    String identityNo,
    String placeOfBirth,
    String gender,
    String religion,
    String blood,
    String status,
    String identityAddress,
    String address,
    String email,
    String phone,
    String reference,
    String skill,
    String placement,
    String expectedSalary
    
  ) async {
    FormData data = FormData.fromMap({
      "position": position,
      "name": name,
      "identity_no": identityNo,
      "place_of_birth": placeOfBirth,
      "gender": gender,
      "religion": religion,
      "status" : status,
      "blood_type": blood,
      "identity_address": identityAddress,
      "address": address,
      "email": email,
      "phone": phone,
      "reference_number": reference,
      "skill" : skill,
      "placement": placement,
      "expected_salary" : expectedSalary,
    });

    return await post('/employee/update',
        data: data, needsToken: false, pure: true);
  }
}