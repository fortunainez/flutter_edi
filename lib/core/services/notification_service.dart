import 'package:dio/dio.dart';
import 'package:apdi/core/models/api_response_model.dart';
import 'package:apdi/core/models/notification_model.dart';
import 'package:apdi/core/services/http_connection.dart';

class NotificationService extends HttpConnection {
  Future list(
    int start
  ) async {
    ApiResponseModel res = await get('/notification/list?start=$start');
    if (res.status == 200) {
      List<NotificationModel> data = [];
      res.data.forEach((el) {
        data.add(NotificationModel.fromJson(el));
      });
      return data;
    }
  }
  Future unRead() async {
    return await get('/notification/unread',pure: true);
  }
  Future edit(
    int id,
  ) async {
    FormData data = FormData.fromMap({
      "id": id,
    });

    return await post('/notification/edit',
        data: data, pure: true);
  }
}
