import 'package:get/get.dart';
import 'package:apdi/core/models/slider_model.dart';
import 'package:apdi/core/services/slider_service.dart';

class SliderController extends GetxController {
  static SliderController get to => Get.find();
  List<SliderModel> sliders = [];
  final _sliderService = SliderService();
  Future getList() async {
    sliders = await _sliderService.list();
    update();
  }
}
