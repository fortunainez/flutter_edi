import 'package:get/get.dart';
import 'package:apdi/core/models/trade_model.dart';
import 'package:apdi/core/services/trade_service.dart';

class TradeController extends GetxController {
  static TradeController get to => Get.find();
  List<TradeModel> trades = [];
  final _tradeService = TradeService();
  Future getList() async {
    return await _tradeService.list();
  }
}
