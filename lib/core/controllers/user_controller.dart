import 'package:apdi/ui/screens/employee.dart';
import 'package:apdi/ui/screens/home.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;
import 'package:apdi/core/models/user_model.dart';
import 'package:apdi/core/services/user_service.dart';
import 'package:apdi/core/utils/dialog_util.dart';
import 'package:apdi/core/utils/storage_util.dart';
import 'package:apdi/ui/screens/main.dart';
import 'package:apdi/ui/screens/login.dart';

class UserController extends GetxController {
  static UserController get to => Get.find();
  UserModel? user;
  final _userService = UserService();
  List<UserModel> users = [];

  @override
  void onInit() async {
    super.onInit();
    if (StorageUtils.to.token != null) {
      await getUser();
      if (user?.name != null) {
        Get.offAll(() => const MainScreen());
      }
    } else {
      nosession();
    }
  }

  Future<void> nosession() async {
    Future.delayed(
      const Duration(milliseconds: 1500),
      () => Get.offAll(() => const LoginScreen()),
    );
  }

  Future getUser() async {
    dio.Response res = await _userService.me();
    if (res.statusCode == 200) {
      user = UserModel.fromJson(res.data['data']);
      return user;
    }
    if (res.statusCode == 422) {
      nosession();
    }
  }

  Future login({
    required String email,
    required String password,
  }) async {
    DialogUtils.showLoading('Masuk...');
    dio.Response res = await _userService.login(email, password);
    //* Throw errors
    if (res.statusCode! >= 400) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
      return;
    }

    if (res.statusCode == 200) {
      //* Save token
      var token = res.data['token'];
      StorageUtils.to.saveToken(token);

      await getUser();
      Get.offAll(() => const MainScreen());
    } else {
      DialogUtils.showWarning(res.data['errors'], closePreDialog: true);
      return;
    }
  }

  Future register({
    required String name,
    required String email,
    required String password,
  }) async {
    DialogUtils.showLoading('Laoding...');
    dio.Response res = await _userService.register(name, email, password);
    //* Throw errors
    if (res.statusCode! >= 400) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
      return;
    }

    if (res.statusCode == 200) {
      //* Save token
      Get.to(() => const LoginScreen());
    } else {
      DialogUtils.showWarning(res.data['errors'], closePreDialog: true);
      return;
    }
  }

  void logout() async {
    DialogUtils.showChoose(
        'Are you sure you want to logout from application?', 'Yes',
        onClick: () async {
      // stop cron
      await StorageUtils.to.storage.erase();
      Get.offAll(() => const LoginScreen());
    });
  }

  Future updateStatus({
    required int userId,
    required int statusActive,
  }) async {
    DialogUtils.showLoading('Laoding...');
    dio.Response res = await _userService.updatestatus(userId, statusActive);
    //* Throw errors
    if (res.statusCode! >= 400) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
      return;
    }

    if (res.statusCode == 200) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
    } else {
      DialogUtils.showWarning(res.data['errors'], closePreDialog: true);
      return;
    }
  }

  Future deleteEmployee({required int employeeId}) async {
    DialogUtils.showLoading('Laoding...');
    dio.Response res = await _userService.deleteemployee(employeeId);
    //* Throw errors
    if (res.statusCode! >= 400) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
      return;
    }
    if (res.statusCode == 200) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
    } else {
      DialogUtils.showWarning(res.data['errors'], closePreDialog: true);
      return;
    }
  }

  Future updateemployee({
    required String position,
    required String name,
    required String identityNo,
    required String placeOfBirth,
    required String gender,
    required String religion,
    required String blod,
    required String status,
    required String identityAddress,
    required String address,
    required String email,
    required String phone,
    required String reference,
    required String skill,
    required String placement,
    required String expectedSalary,
  }) async {
    DialogUtils.showLoading('Laoding...');
    dio.Response res = await _userService.updateemployee(
        position,
        name,
        identityNo,
        placeOfBirth,
        gender,
        religion,
        blod,
        status,
        email,
        identityAddress,
        address,
        phone,
        reference,
        skill,
        placement,
        expectedSalary);
    //* Throw errors
    if (res.statusCode! >= 400) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
      return;
    }

    if (res.statusCode == 200) {
      DialogUtils.showInfo(res.data['message'].toString(),
          closePreDialog: true);
      Get.back();
      getUser();
    } else {
      DialogUtils.showWarning(res.data['errors'], closePreDialog: true);
      return;
    }
  }

  Future getList() async {
    return await _userService.list();
  }

  Future getListemployee() async {
    return await _userService.listemployee();
  }
}
