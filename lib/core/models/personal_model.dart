class PersonalModel {
  int id;
  String name;
  String address;

  PersonalModel({
    required this.id,
    required this.name,
    required this.address
  });

  factory PersonalModel.fromJson(Map<String, dynamic> json) {
    return PersonalModel(
      id: json['id'] ?? "",
      name: json['name'] ?? "",
      address: json['address'] ?? ""
    );
  }
}