class TradeModel {
  int id;
  String order;
  String status;
  String nominal;
  String type;

  TradeModel(
      {required this.id,
      required this.order,
      required this.status,
      required this.nominal,
      required this.type,});

  factory TradeModel.fromJson(Map<String, dynamic> json) {
    return TradeModel(
        id: json['id'] ?? "",
        order: json['order'] ?? "",
        status: json['status'] ?? "",
        nominal: json['nominal'] ?? "",
        type: json['type'] ?? "",);
  }
}
