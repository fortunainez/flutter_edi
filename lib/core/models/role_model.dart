class RoleModel {
  int id;
  String name;
  String displayName;

  RoleModel({
    required this.id,
    required this.name,
    required this.displayName
  });

  factory RoleModel.fromJson(Map<String, dynamic> json) {
    return RoleModel(
      id: json['id'] ?? "",
      name: json['name'] ?? "",
      displayName: json['display_name'] ?? ""
    );
  }
}