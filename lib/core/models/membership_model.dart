class MembershipModel {
  int id;
  String code;
  String startDate;
  String endDate;
  String type;
  int status;

  MembershipModel({
    required this.id,
    required this.code,
    required this.startDate,
    required this.endDate,
    required this.type,
    required this.status
  });

  factory MembershipModel.fromJson(Map<String, dynamic> json) {
    return MembershipModel(
      id: json['id'] ?? "",
      code: json['code'] ?? "",
      startDate: json['start_date'] ?? "",
      endDate: json['end_date'] ?? "",
      type: json['type'] ?? "",
      status: json['status'] ?? 0,
    );
  }
}