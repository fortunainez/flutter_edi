class ApiResponseModel<T> {
  ApiResponseModel({
    required this.status,
    required this.data,
    required this.errors,
    required this.message,
  });

  int status;
  T errors;
  T data;
  String message;

  factory ApiResponseModel.fromJson(Map<String, dynamic> json) => ApiResponseModel(
        status: json["status"],
        message: json["message"] ?? "",
        data: json["data"] ?? "",
        errors: json["errors"] ?? "",
      );
}