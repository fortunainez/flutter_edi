import 'package:apdi/core/models/employee_model.dart';
import 'package:apdi/core/models/membership_model.dart';
import 'package:apdi/core/models/role_model.dart';

class UserModel {
  int id;
  String name;
  String email;
  String role;
  String status;
  String greeting;
  EmployeeModel? employee;


  UserModel({
    required this.id,
    required this.name,
    required this.email,
    required this.role,
    required this.status,
    required this.greeting,
    required this.employee,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'] ?? "",
      name: json['name'] ?? "",
      email: json['email'] ?? "",
      role: json['role'] ?? "",
      status: json['status'].toString(),
      greeting: json['greeting'] ?? "",
       employee: json["employee"] != null
          ? EmployeeModel.fromJson(json["employee"])
          : null,
    );
  }
}
