class SliderModel {
  int id;
  String image;

  SliderModel(
      {required this.id,
      required this.image,});

  factory SliderModel.fromJson(Map<String, dynamic> json) {
    return SliderModel(
        id: json['id'] ?? "",
        image: json['image'] ?? "",);
  }
}
