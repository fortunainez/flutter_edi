import 'package:flutter/material.dart';
import 'package:dio/dio.dart' as dio;
import 'package:apdi/core/services/notification_service.dart';

class NotificationModel extends ChangeNotifier {
  int? unRead = 0;
  int? id;
  String? type;
  String? description;
  int? referenceId;
  int? read;
  String? createdAt;

  NotificationModel({
    this.id,
    this.type,
    this.description,
    this.referenceId,
    this.read,
    this.createdAt,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
        id: json['id'] ?? "",
        type: json['type'] ?? "",
        description: json['description'] ?? "",
        referenceId: json['reference_id'] ?? "",
        read: json['read'] ?? "",
        createdAt: json['created_at'] ?? "");
  }
  Future<void> getRead() async {
    dio.Response res = await NotificationService().unRead();
    if (res.statusCode! >= 400) {
      unRead = 0;
      notifyListeners();
    } else {
      unRead = res.data['total'];
      notifyListeners();
    }
  }
}
