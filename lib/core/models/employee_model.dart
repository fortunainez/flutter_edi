class EmployeeModel {
  int id;
  String position;
  String name;
  String identityNo;
  String placeOfBirth;
  String gender;
  String religion;
  String blood;
  String status;
  String identityAddress;
  String address;
  String email;
  String phone;
  String reference;
  String skill;
  String placement;
  int expectedSalary;
  EmployeeModel(
      {
        required this.id,
        required this.position,
        required this.name,
        required this.identityNo,
        required this.placeOfBirth,
        required this.gender,
        required this.religion,
        required this.blood,
        required this.status,
        required this.email,
        required this.identityAddress,
        required this.address,
        required this.phone,
        required this.reference,
        required this.skill,
        required this.placement,
        required this.expectedSalary 
      });

  factory EmployeeModel.fromJson(Map<String, dynamic> json) {
    return EmployeeModel(
      id: json['id'] ?? "",
      position: json['position'] ?? "",
      name: json['name'] ?? "",
      identityNo: json['identity_no'] ?? "",
      placeOfBirth: json['place_of_birth'] ?? "",
      gender: json['gender'] ?? "",
      religion: json['religion'] ?? "",
      blood: json['blood_type'] ?? "",
      status: json['status'] ?? "",
      email: json['email'] ?? "",
      identityAddress: json['identity_address'] ?? "",
      address: json['address'] ?? "",
      phone: json['phone'] ?? "",
      reference: json['reference_number'] ?? "",
      skill: json['skill'] ?? "",
      placement: json['placement'] ?? "",
      expectedSalary: json['expected_salary'] ?? 0,
    );
  }
}
