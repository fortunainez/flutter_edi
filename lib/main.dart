import 'dart:io';

import 'package:apdi/core/controllers/slider_controller.dart';
import 'package:apdi/core/controllers/trade_controller.dart';
import 'package:apdi/core/models/notification_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:apdi/ui/screens/splash.dart';
import 'package:apdi/core/constant/constant.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:apdi/core/utils/storage_util.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';


class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
void main() async {
  await GetStorage.init();
  await Hive.initFlutter();
  await Hive.openBox('appBox');
  Get.put(StorageUtils());
  Get.put(UserController());
  Get.put(SliderController());
  Get.put(TradeController());
  HttpOverrides.global = MyHttpOverrides();
   runApp( MultiProvider(
      providers: [
        // ChangeNotifierProvider(create: (context) => CartModel()),
        // ChangeNotifierProvider(create: (context) => NavigationModel()),
        // ChangeNotifierProvider(create: (context) => ChatModel()),
        ChangeNotifierProvider(create: (context) => NotificationModel()),
      ],
      child: const MyApp(),
    ),);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'APDI',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor:  Styles.primaryColor,
          scaffoldBackgroundColor: Colors.white,
          visualDensity: VisualDensity.adaptivePlatformDensity, colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Styles.primaryColor),
        ),
        builder: (context, child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
            child: child!,
          );
        },
        home: const SplashScreen(),
      );
  }
}