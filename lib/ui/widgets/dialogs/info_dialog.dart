import 'package:apdi/core/constant/constant.dart';
import 'package:flutter/material.dart';

class InfoDialog extends StatelessWidget {
  final String text;
  final String clickText;
  final IconData icon;
  final Function onClickOK;
  final Function? onClickCancel;

  const InfoDialog(
      {super.key, required this.text,
      required this.icon,
      required this.onClickOK,
      this.onClickCancel,
      this.clickText = "OK"});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        // title: Center(
        //   child: Icon(
        //     icon,
        //     color: Styles.primaryColor,
        //     size: 36,
        //   ),
        // ),
        content: Text(text,
            style: const TextStyle(
              color: Colors.black87,
              fontSize: 16,
            )),
        actions: <Widget>[
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(Styles.primaryColor),
            ),
            onPressed: () => onClickOK(),
            child: Text(
              clickText,
              style:
                  const TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
            ),
          ),
          onClickCancel != null
              ? TextButton(
                  onPressed: () => onClickCancel!(),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                    side: 
                    MaterialStateProperty.all(
                      const BorderSide(color: Styles.primaryColor)
                    ),
                    
                  ),
                  //shape: RoundedRectangleBorder(
                  //borderRadius: BorderRadius.circular(5)),
                  //borderSide: BorderSide(color: Styles.primaryColor),
                  child: const Text(
                    "Batal",
                    style: TextStyle(
                        color: Styles.primaryColor,
                        fontWeight: FontWeight.w600),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }
}
