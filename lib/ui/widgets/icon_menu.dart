import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class IconMenu extends StatelessWidget {
  const IconMenu(
      {Key? key,
      required this.title,
      required this.image,
      this.size = 50,
      this.flexible = false})
      : super(key: key);

  final String title;
  final String image;
  final double size;
  final bool flexible;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.all(10),
            child: Image(
              height: size,
              image: AssetImage(image),
            )),
        const SizedBox(height: 5),
        flexible
            ? Flexible(
                child: AutoSizeText(
                  title,
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width / 30,
                    fontFamily: 'Sanomat Grab Web',
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            : Text(
                title,
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width / 30,
                  fontFamily: 'Sanomat Grab Web',
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              )
      ],
    );
  }
}

class MoreIconMenu extends StatelessWidget {
  const MoreIconMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10),
          child: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(30)),
            child: Icon(
              Icons.more_horiz,
              size: 40,
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
        const SizedBox(height: 5),
        Text("Lainya",
            style: TextStyle(
              fontSize: MediaQuery.of(context).size.width / 30,
              fontFamily: 'Sanomat Grab Web',
              color: Colors.black,
            )),
        // Spacer(),
      ],
    );
  }
}

class ShimmerIconMenu extends StatelessWidget {
  const ShimmerIconMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25), color: Colors.white),
        ),
        const SizedBox(height: 5),
        Container(
          height: 10,
          width: 50,
          decoration: const BoxDecoration(color: Colors.white),
        ),
      ],
    );
  }
}

class ShimmerThumbnail extends StatelessWidget {
  const ShimmerThumbnail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 80,
          width: 80,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5), color: Colors.white),
        ),
      ],
    );
  }
}

class NetworkIconMenu extends StatelessWidget {
  const NetworkIconMenu(
      {Key? key,
      required this.title,
      required this.image,
      this.size = 50,
      this.flexible = false})
      : super(key: key);

  final String title;
  final String image;
  final double size;
  final bool flexible;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.all(10),
            child: CachedNetworkImage(
              placeholder: (context, url) {
                return Image.asset(
                  'assets/images/placeholder-produk.jpg',
                  fit: BoxFit.cover,
                );
              },
              imageUrl: image,
              height: size,
              width: size,
              fit: BoxFit.cover,
            )),
        const SizedBox(height: 5),
        flexible
            ? Flexible(
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width / 32,
                    fontFamily: 'Sanomat Grab Web',
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            : Text(
                title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width / 32,
                  fontFamily: 'Sanomat Grab Web',
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              )
      ],
    );
  }
}
