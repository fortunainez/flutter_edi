
import 'package:flutter/material.dart';

class BottomNavigationItem extends StatelessWidget {
  const BottomNavigationItem({
    required this.icon,
    this.iconActive,
    required this.title,
    this.isActive = false,
    this.onTap,
    Key? key,
  }) : super(key: key);

  final String title;
  final IconData icon;
  final IconData? iconActive;
  final VoidCallback? onTap;
  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Material(
        color: Colors.white,
        child: Container(
          padding: const EdgeInsets.all(7),
          child: Column(
            children: <Widget>[
              isActive
                  ? iconActive != null
                      ? Icon(
                          iconActive,
                          color: isActive
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                        )
                      : Icon(
                          icon,
                          color: isActive
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                        )
                  : Icon(
                      icon,
                      color: isActive
                          ? Theme.of(context).primaryColor
                          : Colors.grey,
                    ),
              const Spacer(),
              Text(title,
                  style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width / 40,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Sanomat Grab Web',
                    color:
                        isActive ? Theme.of(context).primaryColor : Colors.grey,
                  )),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}