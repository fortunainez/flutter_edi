import 'package:apdi/core/constant/constant.dart';
import 'package:apdi/core/models/employee_model.dart';
import 'package:apdi/core/models/user_model.dart';
import 'package:apdi/core/utils/dialog_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:flutter/gestures.dart';
import 'dart:async';

import 'package:apdi/ui/screens/login.dart';
import 'package:shimmer/shimmer.dart';

class ListUserScreen extends StatefulWidget {
  const ListUserScreen({
    Key? key,
  }) : super(key: key);

  @override
  ListUserScreenState createState() => ListUserScreenState();
}

class ListUserScreenState extends State<ListUserScreen> {
  List<UserModel> users = [];
  bool usersLoaded = false;
  bool isLoaded = false;
  UserModel? user;
  @override
  void initState() {
    super.initState();
    getAllusers();
    getUser();
  }

  getUser() async {
    user = await UserController.to.getUser();
    setState(() {
      isLoaded = true;
    });
  }

  Future getAllusers() async {
    List<UserModel> getusers = await UserController.to.getList();
    setState(() {
      users = getusers;
      usersLoaded = true;
    });
  }

  Future refreshData() async {
    setState(() {
      isLoaded = false;
    });
    getAllusers();
    getUser();
  }

  Widget _divider() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(height: size.height * 0.01);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    bool usersExist = users.isNotEmpty;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Pilih User",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: InkWell(
          borderRadius: BorderRadius.circular(10.0),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onTap: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton.small(
        onPressed: () {
          // Add your onPressed code here!
        },
        child: const Icon(Icons.add),
      ),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: refreshData,
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: usersLoaded == true
                      ? usersExist == true
                          ? // if we have contacts to show
                          ListView.builder(
                              itemCount: users.length,
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, index) => Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(children: <Widget>[
                                    Expanded(
                                      flex: 7,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5),
                                            child: Text(
                                              users[index].name,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontFamily: 'Sanomat Grab Web',
                                                fontWeight: FontWeight.bold,
                                                fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    30,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5),
                                            child: Text(
                                              users[index].email,
                                              style: TextStyle(
                                                  fontFamily:
                                                      'Sanomat Grab Web',
                                                  color: Colors.black45,
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          34),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5.0),
                                            child: Text(
                                              users[index].role,
                                              style: TextStyle(
                                                  fontFamily:
                                                      'Sanomat Grab Web',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          32),
                                            ),
                                          ),
                                          Text(
                                            users[index].status == '1'
                                                ? 'Active'
                                                : 'Inactive',
                                            style: TextStyle(
                                                fontFamily: 'Sanomat Grab Web',
                                                color:
                                                    users[index].status == '1'
                                                        ? Colors.green
                                                        : Colors.red,
                                                fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    32,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                              scrollDirection: Axis.vertical,
                            )
                          : SvgPicture.asset(
                              'assets/images/empty.svg',
                            )
                      : ListView.builder(
                          itemCount: 5,
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) => Card(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey.shade300,
                                highlightColor: Colors.grey.shade100,
                                child: Row(children: <Widget>[
                                  Expanded(
                                    flex: 7,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 5),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 5, right: 150),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 100),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 5.0, left: 10),
                                          child: Container(
                                            height: 15,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 30),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ]),
                              ),
                            ),
                          ),
                          scrollDirection: Axis.vertical,
                        ),
                ),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
