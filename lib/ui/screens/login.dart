import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:apdi/core/utils/validate_util.dart';
import 'package:apdi/ui/screens/register.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final cEmail = TextEditingController();
  final cPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _showPassword = false;
  int _state = 0;
  @override
  void initState() {
    super.initState();
  }

  Widget _logo() {
    return Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 40.0),
        child: Image.asset(
          "assets/images/logo.png",
          width: 300,
        ));
  }

  Widget _email() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          return ValidateUtils.validateEmail(value!);
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Email',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          prefixIcon: Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.email),
          ),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32,121,181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
        controller: cEmail,
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Widget _password() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        obscureText: !_showPassword,
        validator: (value) {
          return ValidateUtils.requiredField(value!, 'Password harus diisi');
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: InputDecoration(
          hintText: "Password",
          isDense: true,
          prefixIconConstraints: const BoxConstraints(maxHeight: 20),
          prefixIcon: const Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.lock),
          ),
          contentPadding: const EdgeInsets.all(12.0),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32,121,181, 1.0)),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          suffixIconConstraints: const BoxConstraints(maxHeight: 20),
          suffixIcon: Padding(
            padding: const EdgeInsets.only(
              left: 12.0,
              right: 12.0,
            ),
            child: GestureDetector(
              onTap: () {
                setState(() => _showPassword = !_showPassword);
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
              ),
            ),
          ),
        ),
        controller: cPassword,
        textInputAction: TextInputAction.done,
      ),
    );
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return Text(
        "Login",
        textAlign: TextAlign.center,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: MediaQuery.of(context).size.width / 28),
      );
    } else if (_state == 1) {
      return const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return const Icon(Icons.check, color: Colors.white);
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

    Timer(const Duration(milliseconds: 3300), () {
      setState(() {
        _state = 2;
      });
    });
  }

  Widget _login() {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: MaterialButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              FocusScope.of(context).unfocus();
              if (_formKey.currentState!.validate()) {
                UserController.to.login(
                  email: cEmail.text.trim(),
                  password: cPassword.text.trim(),
                );
              }
            }
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          textColor: Colors.white,
          color: const Color.fromRGBO(32,121,181, 1.0),
          height: 44,
          minWidth: double.infinity,
          padding: const EdgeInsets.all(0),
          child: setUpButtonChild()),
    );
  }

  Widget _register() {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: GestureDetector(
          child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
            style: GoogleFonts.roboto(
              fontSize: MediaQuery.of(context).size.width / 28,
              fontWeight: FontWeight.bold,
              color: const Color.fromRGBO(32,121,181, 1.0),
            ),
            children: [
              TextSpan(
                text: 'Dont have account? ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Register',
                style: TextStyle(
                    color: const Color.fromRGBO(32,121,181, 1.0),
                    fontSize: MediaQuery.of(context).size.width / 28),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Get.to(() => const RegisterScreen());
                  },
              ),
            ]),
      )),
    );
  }

  Widget _divider() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(height: size.height * 0.01);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
               Column(
                    children: <Widget>[
                      ClipPath(
                        // clipper: BezierClipper(),
                        child: Container(
                          width: double.infinity,
                          height:  MediaQuery.of(context).size.height * 50 / 100,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color.fromARGB(255, 185, 207, 241),
                               Color.fromARGB(255, 185, 207, 241),
                              ],
                            ),
                          ),
                          child: _logo(),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 50,
                        color: Colors.white,
                      ),
                    ],
                  ),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 37 / 100,left:15,right: 15),
                child: Card(
                   elevation: 10,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                  child: Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 10.0, bottom: 0),
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 40.0, bottom: 40.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _divider(),
                                Text(
                                                "Email",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: size.width / 28,
                                                ),
                                              ),
                                              _divider(),
                                _email(),
                                _divider(),
                                Text(
                                                "Password",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: size.width / 28,
                                                ),
                                              ),
                                              _divider(),
                                _password(),
                                _divider(),
                                _login(),
                                _register()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
