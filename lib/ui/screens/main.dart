import 'package:apdi/ui/screens/account.dart';
import 'package:apdi/ui/screens/home.dart';
import 'package:apdi/ui/screens/listemployee.dart';
import 'package:apdi/ui/widgets/bottom_navigation_item.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';



class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  final List<Widget> _widgetOptions = <Widget>[const HomeScreen(), const AccountScreen()];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       floatingActionButton: FloatingActionButton.extended(
        icon: const Icon(FontAwesomeIcons.users),
        label: Text(
          'Karyawan',
          style: TextStyle(fontSize: MediaQuery.of(context).size.width / 30),
        ),
        onPressed: () {
          Get.to(() =>  const ListEmployeeScreen());
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: SizedBox(
              height: 60.0,
              child: BottomNavigationItem(
                icon: FontAwesomeIcons.compass,
                iconActive: FontAwesomeIcons.solidCompass,
                title: "Home",
                onTap: () {
                  _selectedIndex = 0;
                  setState(() {});
                },
                isActive: _selectedIndex == 0,
              ),
            )),
            const Expanded(child:  Text('')),
            Expanded(
                child: SizedBox(
              height: 60.0,
              child: BottomNavigationItem(
                icon: FontAwesomeIcons.circleUser,
                iconActive: FontAwesomeIcons.solidCircleUser,
                title: "Account",
                onTap: () {
                  _selectedIndex = 1;
                  setState(() {});
                },
                isActive: _selectedIndex == 1,
              ),
            )),
          ],
        ),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return _widgetOptions.elementAt(_selectedIndex);
              },
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }
}
