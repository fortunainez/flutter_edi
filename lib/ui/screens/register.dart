import 'package:apdi/core/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:flutter/gestures.dart';
import 'dart:async';

import 'package:apdi/ui/screens/login.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  RegisterScreenState createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
  final cName = TextEditingController();
  final cEmail = TextEditingController();
  final cPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _showPassword = false;
  int _state = 0;
  @override
  void initState() {
    super.initState();
  }

  Widget _logo() {
    return Container(
       margin: const EdgeInsets.only(bottom: 0, top: 20),
        // alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 40.0),
        child: Image.asset(
          "assets/images/logo.png",
          width: 300,
        ));
  }

  Widget _name() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cName,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Nama',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          prefixIcon: Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.edit),
          ),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _email() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cEmail,
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Email',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          prefixIcon: Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.mail),
          ),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _username() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        keyboardType: TextInputType.phone,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'No Handphone',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          prefixIcon: Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.phone_android),
          ),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _password() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cPassword,
        obscureText: !_showPassword,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: InputDecoration(
          hintText: "Kata Sandi",
          isDense: true,
          prefixIconConstraints: const BoxConstraints(maxHeight: 20),
          prefixIcon: const Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.lock),
          ),
          contentPadding: const EdgeInsets.all(12.0),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          suffixIconConstraints: const BoxConstraints(maxHeight: 20),
          suffixIcon: Padding(
            padding: const EdgeInsets.only(
              left: 15.0,
              right: 15.0,
            ),
            child: GestureDetector(
              onTap: () {
                setState(() => _showPassword = !_showPassword);
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _repassword() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        obscureText: !_showPassword,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: InputDecoration(
          hintText: "Ulangi Kata Sandi",
          isDense: true,
          prefixIconConstraints: const BoxConstraints(maxHeight: 20),
          prefixIcon: const Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.lock),
          ),
          contentPadding: const EdgeInsets.all(12.0),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          suffixIconConstraints: const BoxConstraints(maxHeight: 20),
          suffixIcon: Padding(
            padding: const EdgeInsets.only(
              left: 15.0,
              right: 15.0,
            ),
            child: GestureDetector(
              onTap: () {
                setState(() => _showPassword = !_showPassword);
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return Text(
        "Register",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: MediaQuery.of(context).size.width / 28,
        ),
      );
    } else if (_state == 1) {
      return const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return const Icon(Icons.check, color: Colors.white);
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

    Timer(const Duration(milliseconds: 3300), () {
      setState(() {
        _state = 2;
      });
    });
  }

  Widget _privacy() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: GestureDetector(
          child: RichText(
        textAlign: TextAlign.left,
        text: TextSpan(
            style: GoogleFonts.roboto(
              fontSize: MediaQuery.of(context).size.width / 28,
              color: const Color.fromRGBO(32, 121, 181, 1.0),
            ),
            children: [
              TextSpan(
                text: 'I aggree with ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Terms and Conditions ',
                style: TextStyle(
                    color: const Color.fromRGBO(32, 121, 181, 1.0),
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'and ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Privacy Policy ',
                style: TextStyle(
                    color: const Color.fromRGBO(32, 121, 181, 1.0),
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Asosiasi Pilot Drone Indonesia',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
            ]),
      )),
    );
  }

  Widget _register() {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: MaterialButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              setState(() {
                if (_state == 0) {
                  animateButton();
                }
              });
              FocusScope.of(context).unfocus();
                if (_formKey.currentState!.validate()) {
                  UserController.to.register(
                    name: cName.text.trim(),
                    email: cEmail.text.trim(),
                    password: cPassword.text.trim(),
                  );
              }
            }
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          textColor: Colors.white,
          color: const Color.fromRGBO(32, 121, 181, 1.0),
          height: 44,
          minWidth: double.infinity,
          padding: const EdgeInsets.all(0),
          child: setUpButtonChild()),
    );
  }

  Widget _login() {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: GestureDetector(
          onTap: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const RegisterScreen()))
              },
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                style: GoogleFonts.roboto(
                  fontSize: MediaQuery.of(context).size.width / 28,
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(32, 121, 181, 1.0),
                ),
                children: [
                  TextSpan(
                    text: 'Already have an account? ',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: MediaQuery.of(context).size.width / 28),
                  ),
                  TextSpan(
                    text: 'Login',
                    style: TextStyle(
                        color: const Color.fromRGBO(32, 121, 181, 1.0),
                        fontSize: MediaQuery.of(context).size.width / 28),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        Get.to(() => const LoginScreen());
                      },
                  ),
                ]),
          )),
    );
  }

  Widget _divider() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(height: size.height * 0.01);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Register",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Color.fromARGB(255, 185, 207, 241),
        elevation: 0.0,
        leading: InkWell(
          borderRadius: BorderRadius.circular(10.0),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onTap: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                children: <Widget>[
                  ClipPath(
                    // clipper: BezierClipper(),
                    child: Container(
                       alignment: Alignment.topCenter,
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height * 50 / 100,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color.fromARGB(255, 185, 207, 241),
                            Color.fromARGB(255, 185, 207, 241),
                          ],
                        ),
                      ),
                      child: _logo(),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    color: Colors.white,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 120,
                    left: 15,
                    right: 15,bottom: 40),
                child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 0, bottom: 0),
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 40, bottom: 40.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _divider(),
                                Text(
                                  "Nama",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _name(),
                                _divider(),
                                Text(
                                  "Email",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _email(),
                                _divider(),
                               
                                Text(
                                  "Kata Sandi",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _password(),
                                _divider(),
                                Text(
                                  "Ulangi Kata Sandi",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _repassword(),
                                _divider(),
                                _divider(),
                                SizedBox(height: size.height * 0.01),
                                _privacy(),
                                _register(),
                                _login()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
