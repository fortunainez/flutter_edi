import 'package:apdi/core/constant/constant.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

Widget _logo() {
  return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      child: Image.asset(
        "assets/images/logo.png",
        width: 300,
      ));
}

class SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          "assets/images/bg_splash.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Styles.primaryColor,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [_logo()],
            ),
          ),
        ),
      ],
    );
  }
}
