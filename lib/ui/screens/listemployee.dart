import 'package:apdi/core/constant/constant.dart';
import 'package:apdi/core/models/employee_model.dart';
import 'package:apdi/core/models/user_model.dart';
import 'package:apdi/core/utils/dialog_util.dart';
import 'package:apdi/ui/screens/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:flutter/gestures.dart';
import 'dart:async';

import 'package:apdi/ui/screens/login.dart';
import 'package:shimmer/shimmer.dart';

class ListEmployeeScreen extends StatefulWidget {
  const ListEmployeeScreen({
    Key? key,
  }) : super(key: key);

  @override
  ListEmployeeScreenState createState() => ListEmployeeScreenState();
}

class ListEmployeeScreenState extends State<ListEmployeeScreen> {
  List<EmployeeModel> employees = [];
  bool employeesLoaded = false;
  bool isLoaded = false;
  UserModel? user;
  @override
  void initState() {
    super.initState();
    getAllemployees();
    getUser();
  }

  getUser() async {
    user = await UserController.to.getUser();
    setState(() {
      isLoaded = true;
    });
  }

  Future getAllemployees() async {
    List<EmployeeModel> getemployees =
        await UserController.to.getListemployee();
    setState(() {
      employees = getemployees;
      print(employees.length);
      employeesLoaded = true;
    });
  }

  Future refreshData() async {
    setState(() {
      isLoaded = false;
    });
    getAllemployees();
    getUser();
  }

  Widget _divider() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(height: size.height * 0.01);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    bool employeesExist = employees.isNotEmpty;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Data Pegawai",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: InkWell(
          borderRadius: BorderRadius.circular(10.0),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onTap: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
       
      floatingActionButton:(user!.role == "admin")? FloatingActionButton.small(
        onPressed: () {
         
          Get.to(() => const EmployeeScreen());
        },
        child: const Icon(Icons.add),
      ): Text(''),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: refreshData,
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: employeesLoaded == true
                      ? employeesExist == true
                          ? // if we have contacts to show
                          ListView.builder(
                              itemCount: employees.length,
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, index) => Card(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(children: <Widget>[
                                    Expanded(
                                      flex: 7,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5),
                                            child: Text(
                                              employees[index].name,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontFamily: 'Sanomat Grab Web',
                                                fontWeight: FontWeight.bold,
                                                fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    30,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5),
                                            child: Text(
                                              employees[index].placeOfBirth,
                                              style: TextStyle(
                                                  fontFamily:
                                                      'Sanomat Grab Web',
                                                  color: Colors.black45,
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          34),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 5.0),
                                            child: Text(
                                              employees[index].position,
                                              style: TextStyle(
                                                  fontFamily:
                                                      'Sanomat Grab Web',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                          32),
                                            ),
                                          ),
                                          (user!.role == 'admin')
                                              ? IconButton(
                                                  icon: const Icon(Icons.delete, size: 30, color:Colors.red),
                                                   onPressed: () {
                                                    DialogUtils.showChoose(
                                                        'Are you sure you want to delete data?',
                                                        'Yes',
                                                        onClick: () async {
                                                      // stop cron
                                                      await UserController.to
                                                          .deleteEmployee(
                                                              employeeId:
                                                                  employees[index]
                                                                      .id);
                                                      getAllemployees();
                                                    });
                                                  },
                                                )
                                              : Text(''),
                                        ],
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                              scrollDirection: Axis.vertical,
                            )
                          : SvgPicture.asset(
                              'assets/images/empty.svg',
                            )
                      : ListView.builder(
                          itemCount: 5,
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) => Card(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey.shade300,
                                highlightColor: Colors.grey.shade100,
                                child: Row(children: <Widget>[
                                  Expanded(
                                    flex: 7,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 5),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 5, right: 150),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 100),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 5.0, left: 10),
                                          child: Container(
                                            height: 15,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 30),
                                          child: Container(
                                            height: 12,
                                            color: Colors.black,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ]),
                              ),
                            ),
                          ),
                          scrollDirection: Axis.vertical,
                        ),
                ),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
