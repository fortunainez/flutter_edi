import 'package:apdi/core/constant/constant.dart';
import 'package:apdi/core/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:flutter/gestures.dart';
import 'dart:async';

import 'package:apdi/ui/screens/login.dart';

class EmployeeScreen extends StatefulWidget {
  const EmployeeScreen({
    Key? key,
  }) : super(key: key);

  @override
  EmployeeScreenState createState() => EmployeeScreenState();
}

class EmployeeScreenState extends State<EmployeeScreen> {
  final cPosition = TextEditingController();
  final cName = TextEditingController();
  final cIdentity = TextEditingController();
  final cBirthdate = TextEditingController();
  final cGender = TextEditingController();
  final cRegion = TextEditingController();
  final cBlood = TextEditingController();
  final cStatus = TextEditingController();
  final cIdentityaddress = TextEditingController();
  final cAddress = TextEditingController();
  final cEmail = TextEditingController();
  final cPhone = TextEditingController();
  final cReference = TextEditingController();
  final cSkill = TextEditingController();
  final cPlacement = TextEditingController();
  final cExpectedsalary = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  bool _showPassword = false;
  int _state = 0;
  bool isLoaded = false;
  UserModel? user;
  @override
  void initState() {
    super.initState();
    getUser();
  }

  getUser() async {
    user = await UserController.to.getUser();
    setState(() {
      cPosition.text = user!.employee!.position;
      cName.text = user!.employee!.name;
      cIdentity.text = user!.employee!.identityNo;
      cBirthdate.text = user!.employee!.placeOfBirth;
      cGender.text = user!.employee!.gender;
      cRegion.text = user!.employee!.religion;
      cBlood.text = user!.employee!.blood;
      cStatus.text = user!.employee!.status;
      cIdentityaddress.text = user!.employee!.identityAddress;
      cAddress.text = user!.employee!.address;
      cEmail.text = user!.employee!.email;
      cPhone.text = user!.employee!.phone;
      cReference.text = user!.employee!.reference;
      cSkill.text = user!.employee!.skill;
      cPlacement.text = user!.employee!.placement;
      cExpectedsalary.text = user!.employee!.expectedSalary.toString();

      isLoaded = true;
    });
  }

  Widget _logo() {
    return Container(
        margin: const EdgeInsets.only(bottom: 0, top: 20),
        // alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 40.0),
        child: Image.asset(
          "assets/images/logo.png",
          width: 300,
        ));
  }

  Widget _position() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cPosition,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Posisi yang dilamar',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _placement() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cPlacement,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Ya/Tidak',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _expectedSalary() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cExpectedsalary,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: '9000000',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _name() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cName,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Nama',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _identity() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cIdentity,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'No.KTP',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _region() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cRegion,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Agama', 
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _skill() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cSkill,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Skill',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _birthdate() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cBirthdate,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Tempat, Tanggal Lahir',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _blood() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cBlood,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Golongan Darah',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _status() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cStatus,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Status',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _gender() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cGender,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Jenis Kelamin',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _identityadress() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cIdentityaddress,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Alamat KTP',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _address() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cAddress,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Alamat Tinggal',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _email() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cEmail,
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Email',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _phone() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cPhone,
        keyboardType: TextInputType.phone,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'No Handphone',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          prefixIcon: Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.phone_android),
          ),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _reference() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        controller: cReference,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: const InputDecoration(
          hintText: 'Orang Terdekat Yang Dapat Dihubungi',
          isDense: true,
          prefixIconConstraints: BoxConstraints(maxHeight: 20),
          contentPadding: EdgeInsets.all(12.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      ),
    );
  }

  Widget _repassword() {
    return Container(
      alignment: Alignment.center,
      child: TextFormField(
        obscureText: !_showPassword,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter some text';
          }
          return null;
        },
        onChanged: (value) {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
          }
        },
        style: TextStyle(
          fontSize: MediaQuery.of(context).size.width / 28,
          fontFamily: 'Sanomat Grab Web',
        ),
        decoration: InputDecoration(
          hintText: "Ulangi Kata Sandi",
          isDense: true,
          prefixIconConstraints: const BoxConstraints(maxHeight: 20),
          prefixIcon: const Padding(
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            child: Icon(Icons.lock),
          ),
          contentPadding: const EdgeInsets.all(12.0),
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide:
                BorderSide(color: Color.fromRGBO(0, 0, 0, 0.20), width: 1),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Color.fromRGBO(32, 121, 181, 1.0)),
          ),
          errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
          suffixIconConstraints: const BoxConstraints(maxHeight: 20),
          suffixIcon: Padding(
            padding: const EdgeInsets.only(
              left: 15.0,
              right: 15.0,
            ),
            child: GestureDetector(
              onTap: () {
                setState(() => _showPassword = !_showPassword);
              },
              child: Icon(
                _showPassword ? Icons.visibility : Icons.visibility_off,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return Text(
        "Simpan",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: MediaQuery.of(context).size.width / 28,
        ),
      );
    } else if (_state == 1) {
      return const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return const Icon(Icons.check, color: Colors.white);
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

    Timer(const Duration(milliseconds: 3300), () {
      setState(() {
        _state = 2;
      });
    });
  }

  Widget _privacy() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: GestureDetector(
          child: RichText(
        textAlign: TextAlign.left,
        text: TextSpan(
            style: GoogleFonts.roboto(
              fontSize: MediaQuery.of(context).size.width / 28,
              color: const Color.fromRGBO(32, 121, 181, 1.0),
            ),
            children: [
              TextSpan(
                text: 'I aggree with ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Terms and Conditions ',
                style: TextStyle(
                    color: const Color.fromRGBO(32, 121, 181, 1.0),
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'and ',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Privacy Policy ',
                style: TextStyle(
                    color: const Color.fromRGBO(32, 121, 181, 1.0),
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
              TextSpan(
                text: 'Asosiasi Pilot Drone Indonesia',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: MediaQuery.of(context).size.width / 28),
              ),
            ]),
      )),
    );
  }

  Widget _register() {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: MaterialButton(
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              setState(() {
                if (_state == 0) {
                  animateButton();
                }
              });
              FocusScope.of(context).unfocus();
              if (_formKey.currentState!.validate()) {
                await UserController.to.updateemployee(
                  position: cPosition.text.trim(),
                  name: cName.text.trim(),
                  identityNo: cIdentity.text.trim(),
                  placeOfBirth: cBirthdate.text.trim(),
                  gender: cGender.text.trim(),
                  religion: cRegion.text.trim(),
                  blod: cBlood.text.trim(),
                  status: cStatus.text.trim(),
                  email: cEmail.text.trim(),
                  identityAddress: cIdentityaddress.text.trim(),
                  address: cAddress.text.trim(),
                  phone: cPhone.text.trim(),
                  reference: cReference.text.trim(),
                  skill: cSkill.text.trim(),
                  placement: cPlacement.text.trim(),
                  expectedSalary: cExpectedsalary.text.trim(),
                );
                Get.back();
              }
            }
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          textColor: Colors.white,
          color: const Color.fromRGBO(32, 121, 181, 1.0),
          height: 44,
          minWidth: double.infinity,
          padding: const EdgeInsets.all(0),
          child: setUpButtonChild()),
    );
  }

  Widget _divider() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(height: size.height * 0.01);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Data Calon Pegawai",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: InkWell(
          borderRadius: BorderRadius.circular(10.0),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onTap: () {
            Get.back();
          },
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 15, right: 15, bottom: 10),
                child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 0, bottom: 0),
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 40, bottom: 40.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _divider(),
                                Text(
                                  "Posisi yang dilamar",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _position(),
                                _divider(),
                                Text(
                                  "Nama",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _name(),
                                _divider(),
                                Text(
                                  "No.KTP",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _identity(),
                                _divider(),
                                Text(
                                  "Tempat, Tanggal Lahir",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _birthdate(),
                                _divider(),
                                Text(
                                  "Jenis Kelamin",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _gender(),
                                _divider(),
                                Text(
                                  "Agama",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _region(),
                                _divider(),
                                Text(
                                  "Golongan Darah",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _blood(),
                                _divider(),
                                Text(
                                  "Status",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _status(),
                                _divider(),
                                Text(
                                  "Almat KTP",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _identityadress(),
                                _divider(),
                                Text(
                                  "Alamat Tinggal",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _address(),
                                _divider(),
                                Text(
                                  "Email",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _email(),
                                _divider(),
                                Text(
                                  "No.Telp",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _phone(),
                                _divider(),
                                Text(
                                  "Orang terdekat yang dapat dihubungi",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _reference(),
                                _divider(),
                                Text(
                                  "Skill",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _skill(),
                                _divider(),
                                Text(
                                  "Bersedia ditempatkan diseluruh kantor perusahaan",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _placement(),
                                _divider(),
                                Text(
                                  "Penghasilkan yang dihasilkan",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.width / 28,
                                  ),
                                ),
                                _divider(),
                                _expectedSalary(),
                                _divider(),
                                _divider(),
                                SizedBox(height: size.height * 0.01),
                                _register(),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
