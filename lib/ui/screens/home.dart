import 'dart:async';

import 'package:apdi/core/constant/constant.dart';
import 'package:apdi/core/controllers/trade_controller.dart';
import 'package:apdi/core/controllers/user_controller.dart';
import 'package:apdi/core/models/notification_model.dart';
import 'package:apdi/core/models/trade_model.dart';
import 'package:apdi/core/models/user_model.dart';
import 'package:apdi/core/utils/dialog_util.dart';
import 'package:apdi/core/utils/flash_util.dart';
import 'package:apdi/ui/screens/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:lecle_bubble_timeline/lecle_bubble_timeline.dart';
import 'package:lecle_bubble_timeline/models/timeline_item.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  List<UserModel> users = [];
  bool usersLoaded = false;
  bool isLoaded = false;
  UserModel? user;
  @override
  void initState() {
    super.initState();
    getUser();
    getAllUsers();
    //init();
  }

  getUser() async {
    user = await UserController.to.getUser();
    setState(() {
      isLoaded = true;
    });
  }

  Future getAllUsers() async {
    List<UserModel> getusers = await UserController.to.getList();
    setState(() {
      users = getusers;
      print(users.length);
      usersLoaded = true;
    });
  }

  Future refreshData() async {
    setState(() {
      isLoaded = false;
    });
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    bool usersExist = users.isNotEmpty;
    return RefreshIndicator(
      onRefresh: refreshData,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(
                  15, 15 + MediaQuery.of(context).viewPadding.top, 15, 15),
              child: isLoaded
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundColor: Theme.of(context).primaryColor,
                          radius: 22,
                          child: CircleAvatar(
                            radius: 21,
                            backgroundImage: NetworkImage(
                                'https://cdn3.vectorstock.com/i/1000x1000/30/97/flat-business-man-user-profile-avatar-icon-vector-4333097.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                        ),
                        const SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                user!.name,
                                //'devicePixelRatio: ${queryData.textScaleFactor}',
                                style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width / 22,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Sanomat Grab Web',
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.all(2),
                              ),
                              // Row(
                              //   children: [
                              //     Text(
                              //       'Member No : ${user!.membership != null ? user!.membership!.code : 'Not Registered'}',
                              //       style: TextStyle(
                              //         fontSize:
                              //             MediaQuery.of(context).size.width /
                              //                 32,
                              //         color: Colors.grey[600],
                              //         fontFamily: 'Sanomat Grab Web',
                              //       ),
                              //     ),
                              //     const SizedBox(width: 10),
                              //     user!.membership != null
                              //         ? GestureDetector(
                              //             onTap: () {
                              //               Clipboard.setData(
                              //                 ClipboardData(text: user!.email),
                              //               ).then((_) {
                              //                 FlashUtils.defaultFlash(
                              //                     context: context,
                              //                     title: "Salin",
                              //                     message:
                              //                         "Kode berhasil disalin");
                              //               });
                              //             },
                              //             child: const Icon(
                              //               Icons.copy,
                              //               size: 15,
                              //             ),
                              //           )
                              //         : const SizedBox(),
                              //   ],
                              // )
                              // Row(
                              //   children: [
                              //     Text(
                              //       'Member No : ${user!.membership != null ? user!.membership!.code : 'Not Registered'}',
                              //       style: TextStyle(
                              //         fontSize:
                              //             MediaQuery.of(context).size.width /
                              //                 32,
                              //         color: Colors.grey[600],
                              //         fontFamily: 'Sanomat Grab Web',
                              //       ),
                              //     ),
                              //     const SizedBox(width: 10),
                              //     user!.membership != null
                              //         ? GestureDetector(
                              //             onTap: () {
                              //               Clipboard.setData(
                              //                 ClipboardData(text: user!.email),
                              //               ).then((_) {
                              //                 FlashUtils.defaultFlash(
                              //                     context: context,
                              //                     title: "Salin",
                              //                     message:
                              //                         "Kode berhasil disalin");
                              //               });
                              //             },
                              //             child: const Icon(
                              //               Icons.copy,
                              //               size: 15,
                              //             ),
                              //           )
                              //         : const SizedBox(),
                              //   ],
                              // )
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            // Get.to(const NotificationScreen());
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Align(
                                alignment: Alignment.topRight,
                                child: Stack(
                                  children: <Widget>[
                                    const Icon(
                                      FontAwesomeIcons.bell,
                                    ),
                                    Consumer<NotificationModel>(builder:
                                        (context, notification, child) {
                                      return notification.unRead! > 0
                                          ? Positioned(
                                              right: 0,
                                              child: Container(
                                                padding:
                                                    const EdgeInsets.all(1),
                                                decoration: BoxDecoration(
                                                  color: Colors.red,
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                ),
                                                constraints:
                                                    const BoxConstraints(
                                                  minWidth: 12,
                                                  minHeight: 12,
                                                ),
                                              ),
                                            )
                                          : const SizedBox();
                                    })
                                  ],
                                )),
                          ),
                        ),
                      ],
                    )
                  : Shimmer.fromColors(
                      baseColor: Colors.grey.shade300,
                      highlightColor: Colors.grey.shade100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircleAvatar(
                            backgroundColor: Theme.of(context).primaryColor,
                            radius: 22,
                            child: const CircleAvatar(
                              radius: 21,
                              backgroundImage: NetworkImage(
                                'https://apdi.id/assets/images/logo.png',
                              ),
                              backgroundColor: Colors.transparent,
                            ),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.65,
                                    height: 12,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white),
                                  )
                                ],
                              ),
                              const Padding(
                                padding: EdgeInsets.all(2),
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    height: 12,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10, left: 10),
              child: Card(
                // color: const Color(0xFF605ca8),
                color: Styles.primaryColor,
                elevation: 10,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Role",
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              30,
                                          fontFamily: 'Sanomat Grab Web',
                                          color: Colors.white70,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      isLoaded
                                          ? Text(
                                              user!.role.toUpperCase(),
                                              style: TextStyle(
                                                fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    28,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Sanomat Grab Web',
                                                color: Colors.white,
                                              ),
                                            )
                                          : Shimmer.fromColors(
                                              baseColor: Colors.grey.shade300,
                                              highlightColor:
                                                  Colors.grey.shade100,
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.2,
                                                height: 18,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    color: Colors.white),
                                              ),
                                            ),
                                      const SizedBox(width: 10),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                              )),
                          Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: GestureDetector(
                                  onTap: () {
                                    // Get.to(const DepositScreen());
                                  },
                                  child: const Image(
                                    height: 50,
                                    image: AssetImage(
                                        "assets/images/certificate.png"),
                                  ),
                                ),
                              )),
                        ],
                      ),
                      const Divider(
                        thickness: 1,
                        color: Colors.white70,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  user!.email,
                                  style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width / 30,
                                    fontFamily: 'Sanomat Grab Web',
                                    color: Colors.white70,
                                  ),
                                ),
                                const SizedBox(width: 5),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  user!.status == '1'
                                      ? "Active"
                                      : "Inactive".toUpperCase(),
                                  style: TextStyle(
                                    fontSize:
                                        MediaQuery.of(context).size.width / 30,
                                    fontFamily: 'Sanomat Grab Web',
                                    color: Colors.white70,
                                  ),
                                ),
                                const SizedBox(width: 5),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15.0, bottom: 15.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(4.0)),
                    child: Text(
                      "List User",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: MediaQuery.of(context).size.width / 28),
                    ),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: usersLoaded == true
                  ? usersExist == true
                      ? // if we have contacts to show
                      ListView.builder(
                          itemCount: users.length,
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) => Card(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(children: <Widget>[
                                Expanded(
                                  flex: 7,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          users[index].name,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontFamily: 'Sanomat Grab Web',
                                            fontWeight: FontWeight.bold,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                30,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          users[index].email,
                                          style: TextStyle(
                                              fontFamily: 'Sanomat Grab Web',
                                              color: Colors.black45,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  34),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 3,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 5.0),
                                        child: Text(
                                          users[index].role,
                                          style: TextStyle(
                                              fontFamily: 'Sanomat Grab Web',
                                              fontWeight: FontWeight.bold,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  32),
                                        ),
                                      ),
                                      Text(
                                        users[index].status == '1'
                                            ? 'Active'
                                            : 'Inactive',
                                        style: TextStyle(
                                            fontFamily: 'Sanomat Grab Web',
                                            color: users[index].status == '1'
                                                ? Colors.green
                                                : Colors.red,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                32,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      (user!.role == 'admin') ?
                                          ElevatedButton(
                                              onPressed: () {
                                                DialogUtils.showChoose(
                                                    'Are you sure you want to change status?',
                                                    'Yes', onClick: () async {
                                                  // stop cron
                                                  await UserController.to
                                                      .updateStatus(
                                                          userId:
                                                              users[index].id,
                                                          statusActive: users[
                                                                          index]
                                                                      .status ==
                                                                  '1'
                                                              ? 0
                                                              : 1);
                                                  getAllUsers();
                                                });
                                              },
                                              child:
                                                  users[index].status == '1'
                                                      ? Text('Inactive')
                                                      : Text('Active')):Text(''),
                                        
                                    ],
                                  ),
                                ),
                              ]),
                            ),
                          ),
                          scrollDirection: Axis.vertical,
                        )
                      : SvgPicture.asset(
                          'assets/images/empty.svg',
                        )
                  : ListView.builder(
                      itemCount: 5,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) => Card(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey.shade300,
                            highlightColor: Colors.grey.shade100,
                            child: Row(children: <Widget>[
                              Expanded(
                                flex: 7,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 5),
                                      child: Container(
                                        height: 12,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 5, right: 150),
                                      child: Container(
                                        height: 12,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 100),
                                      child: Container(
                                        height: 12,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 5.0, left: 10),
                                      child: Container(
                                        height: 15,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 30),
                                      child: Container(
                                        height: 12,
                                        color: Colors.black,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),
                      scrollDirection: Axis.vertical,
                    ),
            ),
            const SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }
}
